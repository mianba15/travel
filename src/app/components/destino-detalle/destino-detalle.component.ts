import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

class DestinosApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('llamado por la clase vieja');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id: String): DestinoViaje {
    console.log('Llamado por la clase decoradora!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  //constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {}
  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientViejo) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    //this.destino = null;
    this.destino = this.destinosApiClient.getById(id);
  }

}
