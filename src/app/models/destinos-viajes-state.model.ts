import { Injectable } from '@angular/core';
import { Action, StateObservable } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';

// Estado
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const intializeDestinosViajesState = function() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

//Acciones
export enum DestinosViajesActionTypes {
    Nuevo_destino = '[Destino viajes] Nuevo',
    Elegido_favorito = '[Destino Viajes] Favorito',
    Vote_up = '[Destino viajes] Vote Up',
    Vote_down = '[Destino Viajes] Vote Down' 
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.Nuevo_destino;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.Elegido_favorito;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.Vote_up;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.Vote_down;
    constructor(public destino: DestinoViaje) {}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction;

//Reducers
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
 ): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionTypes.Nuevo_destino: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.Elegido_favorito: {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinosViajesActionTypes.Vote_up: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return { ...state };
        }
        case DestinosViajesActionTypes.Vote_down: {
            const d: DestinoViaje = (action as VoteDownAction).destino;
            d.voteDown();
            return { ...state };
        }
    }
    return state;
}

//Effects
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.Nuevo_destino),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    
    constructor(private actions$: Actions) {}
}